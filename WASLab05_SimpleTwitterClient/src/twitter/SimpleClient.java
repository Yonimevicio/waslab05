package twitter;

import java.util.Date;
import java.util.stream.Stream;

import sun.security.provider.certpath.ResponderId;
import twitter4j.FilterQuery;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

public class SimpleClient {

	public static void main(String[] args) throws Exception {
		
		//final Twitter twitter = new TwitterFactory().getInstance();
		final TwitterStream ts = new TwitterStreamFactory().getInstance();

		
		ts.addListener(new StatusListener() {
		      public void onStatus(Status status) {
		          System.out.println(status.getUser().getName() +
		        		  			 " (@"+status.getUser().getScreenName() +"): "+
		        		  			 status.getText()+"\n"); // print tweet text to console
		      }

			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				// TODO Auto-generated method stub
				
			}});
		FilterQuery tweetFilterQuery = new FilterQuery(); // See 
		tweetFilterQuery.track(new String[]{"#barcelona"}); // OR on keywords
		
		ts.filter(tweetFilterQuery);

		
		
		//ResponseList<Status> lista =  twitter.getUserTimeline("fib_was");
		//twitter.retweetStatus(lista.get(0).getId());
		
		//Date now = new Date();
		//String latestStatus = "Hey @fib_was, we've just completed task #4 [timestamp: "+now+"]";
		//Status status = twitter.updateStatus(latestStatus);
		//System.out.println("Aqui el tweet : " + lista.get(0).getText());       
	}
}
